package com.inventory.spring.web.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.spring.web.dao.Product;
import com.inventory.spring.web.dao.Purchase;
import com.inventory.spring.web.service.ProductService;
import com.inventory.spring.web.service.PurchaseService;

@Controller
public class PurchaseController {

	private PurchaseService purchaseService;

	@Autowired
	public void setPurchaseService(PurchaseService purchaseService) {
		this.purchaseService = purchaseService;
	}

	@RequestMapping("/")
	public String showPurchase() {
		return "purchase";
	}

	@RequestMapping("/createpurchase")
	public String createPurchase(Model model, Principal principal) {
		return null;
	}

	@RequestMapping(value = "/docreatepurchase", method = RequestMethod.POST)
	public String docreatePurchase(Model model, Purchase purchase, BindingResult result, Principal principal) {
		return null;
	}
}
