package com.inventory.spring.web.controllers;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.spring.web.dao.Tax;
import com.inventory.spring.web.service.TaxService;

@Controller
public class TaxController {

	private TaxService taxService;

	@RequestMapping("/")
	public String showTax() {
		return "tax";
	}

	@RequestMapping("/createtax")
	public String createTax(Model model, Principal principal) {
		return null;
	}

	@RequestMapping(value = "/docreatetax", method = RequestMethod.POST)
	public String docreateTaxr(Model model, Tax tax, BindingResult result, Principal principal) {
		return null;
	}
}
