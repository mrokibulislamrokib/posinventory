package com.inventory.spring.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DashBoardController {

	@RequestMapping("/")
	public String showHome() {
		System.out.println("Fron Dashboard Controller");
		return "index";
	}
}
