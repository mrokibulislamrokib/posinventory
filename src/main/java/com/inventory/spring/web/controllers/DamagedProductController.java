package com.inventory.spring.web.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.spring.web.dao.DamagedProduct;
import com.inventory.spring.web.service.CustomerService;
import com.inventory.spring.web.service.DamagedProductService;

@Controller
public class DamagedProductController {
	
	private DamagedProductService damagedproductService;
	
	@Autowired
	public void setCustomerService(DamagedProductService damagedproductService) {
		this.damagedproductService=damagedproductService;
	}
	
	@RequestMapping("/")
	public String showDamagedProduct() {
		return "DamagedProduct";
	}
	
	@RequestMapping("/createdamagedproduct")
	public String  createDamagedProduct(Model model, Principal principal) {
		return null;
	}
	
	@RequestMapping(value="/docreatedamagedproduct", method=RequestMethod.POST)
	public String docreateDamagedProduct(Model model,DamagedProduct damagedproduct, BindingResult result, Principal principal) {
		return null;
	}

}
