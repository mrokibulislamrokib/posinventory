package com.inventory.spring.web.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.spring.web.dao.SubCategory;
import com.inventory.spring.web.service.SubCategoryService;

@Controller
public class SubCategoryController {

	private SubCategoryService subCategoryService;

	@Autowired
	public void setSubCategoryService(SubCategoryService subCategoryService) {
		this.subCategoryService = subCategoryService;
	}

	@RequestMapping("/")
	public String showSubCategory() {
		return "subcategory";
	}

	@RequestMapping("/createsubCategory")
	public String createSubCategory(Model model, Principal principal) {
		return null;
	}

	@RequestMapping(value = "/docreatesubCategory", method = RequestMethod.POST)
	public String docreateSubCategory(Model model, SubCategory subCategory, BindingResult result, Principal principal) {
		return null;
	}

}
