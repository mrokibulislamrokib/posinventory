package com.inventory.spring.web.controllers;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.spring.web.dao.Supplier;
import com.inventory.spring.web.service.SupplierService;

@Controller
public class SupplierController {

	private SupplierService supplierService;

	@RequestMapping("/")
	public String showSupplier() {
		return "supplier";
	}

	@RequestMapping("/createsupplier")
	public String createSupplier(Model model, Principal principal) {
		return null;
	}

	@RequestMapping(value = "/docreatesupplier", method = RequestMethod.POST)
	public String docreateSupplier(Model model, Supplier supplier, BindingResult result, Principal principal) {
		return null;
	}
}
