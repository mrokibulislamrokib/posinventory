package com.inventory.spring.web.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.spring.web.dao.Category;
import com.inventory.spring.web.dao.Customer;
import com.inventory.spring.web.service.CategoryService;
import com.inventory.spring.web.service.CustomerService;

@Controller
public class CustomerController {

	private CustomerService customerService;
	
	@Autowired
	public void setCustomerService(CustomerService customerService) {
		this.customerService=customerService;
	}
	
	@RequestMapping("/")
	public String showCustomer() {
		return "customer";
	}
	
	@RequestMapping("/createcustomer")
	public String  createCustomer(Model model, Principal principal) {
		return null;
	}
	
	@RequestMapping(value="/docreatecustomer", method=RequestMethod.POST)
	public String docreateCustomer(Model model,Customer customer, BindingResult result, Principal principal) {
		return null;
	}
}
