package com.inventory.spring.web.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.spring.web.dao.Order;
import com.inventory.spring.web.service.OrderService;

@Controller
public class OrderController {

	private OrderService orderService;

	@Autowired
	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	@RequestMapping("/")
	public String showOrder() {
		return "order";
	}

	@RequestMapping("/createorder")
	public String createOrder(Model model, Principal principal) {
		return null;
	}

	@RequestMapping(value = "/docreateorder", method = RequestMethod.POST)
	public String docreateOrder(Model model, Order order, BindingResult result, Principal principal) {
		return null;
	}
}
