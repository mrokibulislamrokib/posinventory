package com.inventory.spring.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SettingController {

	@RequestMapping("/")
	public String showSetting() {
		return "setting";
	}
}
