package com.inventory.spring.web.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.spring.web.dao.Employeer;
import com.inventory.spring.web.service.EmployeerService;

@Controller
public class EmployeerController {
	
	private EmployeerService employeerService;

	@Autowired
	public void setEmployeerService(EmployeerService employeerService) {
		this.employeerService = employeerService;
	}

	@RequestMapping("/")
	public String showEmployeer() {
		return "employeer";
	}

	@RequestMapping("/createemployeer")
	public String createEmployeer(Model model, Principal principal) {
		return null;
	}

	@RequestMapping(value = "/docreateemployeer", method = RequestMethod.POST)
	public String docreateEmployeer(Model model, Employeer employeer, BindingResult result, Principal principal) {
		return null;
	}
}
