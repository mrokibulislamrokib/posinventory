package com.inventory.spring.web.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.spring.web.dao.Sale;
import com.inventory.spring.web.service.SaleService;

@Controller
public class SaleController {

	private SaleService saleService;

	@Autowired
	public void setSaleService(SaleService saleService) {
		this.saleService = saleService;
	}

	@RequestMapping("/")
	public String showSale() {
		return "sale";
	}

	@RequestMapping("/createsale")
	public String createSale(Model model, Principal principal) {
		return null;
	}

	@RequestMapping(value = "/docreatesale", method = RequestMethod.POST)
	public String docreateSale(Model model, Sale sale, BindingResult result, Principal principal) {
		return null;
	}
}
