package com.inventory.spring.web.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.spring.web.dao.Category;
import com.inventory.spring.web.service.CategoryService;

@Controller
public class CategoryController {
	
	private CategoryService categoryService;
	
	@Autowired
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService=categoryService;
	}
	
	@RequestMapping("/")
	public String showCategory() {
		return "Category";
	}
	
	@RequestMapping("/createnotice")
	public String  createCategory(Model model, Principal principal) {
		return null;
	}
	
	@RequestMapping(value="/docreatecategory", method=RequestMethod.POST)
	public String docreateCategory(Model model,Category category, BindingResult result, Principal principal) {
		return null;
	}
}
