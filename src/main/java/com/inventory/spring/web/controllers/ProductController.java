package com.inventory.spring.web.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.spring.web.dao.Product;
import com.inventory.spring.web.service.OrderService;
import com.inventory.spring.web.service.ProductService;

@Controller
public class ProductController {
	
	private ProductService productService;

	@Autowired
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	@RequestMapping("/")
	public String showProduct() {
		return "product";
	}

	@RequestMapping("/createproduct")
	public String createProduct(Model model, Principal principal) {
		return null;
	}

	@RequestMapping(value = "/docreateproduct", method = RequestMethod.POST)
	public String docreateProduct(Model model, Product product, BindingResult result, Principal principal) {
		return null;
	}
}
