package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class SubCategoryDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<SubCategory> getSubCategorys() {
		return jdbc.query("", new SubCategoryRowMapper());
	}

	public SubCategory getSubCategory(int id) {
		return (SubCategory) jdbc.query("", new SubCategoryRowMapper());
	}

	public int[] create(List<SubCategory> subCategory) {
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(subCategory.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(SubCategory subCategory) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(subCategory);
		return jdbc.update("", params) == 1;
	}

	public boolean update(SubCategory subCategory) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(subCategory);
		return jdbc.update("", params) == 1;
	}
	
	
	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params) == 1;
	}
}
