package com.inventory.spring.web.dao;

public class Product {

	private int product_id;
	private String serial_number;
	private String category_id;
	private String sub_category_id;
	private String name;
	private String purchase_price;
	private String selling_price;
	private String note;
	private String stock_quantity;

	public Product() {
		super();
	}

	public Product(String serial_number, String category_id, String sub_category_id, String name, String purchase_price,
			String selling_price, String note, String stock_quantity) {
		super();
		this.serial_number = serial_number;
		this.category_id = category_id;
		this.sub_category_id = sub_category_id;
		this.name = name;
		this.purchase_price = purchase_price;
		this.selling_price = selling_price;
		this.note = note;
		this.stock_quantity = stock_quantity;
	}

	public Product(int product_id, String serial_number, String category_id, String sub_category_id, String name,
			String purchase_price, String selling_price, String note, String stock_quantity) {
		super();
		this.product_id = product_id;
		this.serial_number = serial_number;
		this.category_id = category_id;
		this.sub_category_id = sub_category_id;
		this.name = name;
		this.purchase_price = purchase_price;
		this.selling_price = selling_price;
		this.note = note;
		this.stock_quantity = stock_quantity;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getSerial_number() {
		return serial_number;
	}

	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getSub_category_id() {
		return sub_category_id;
	}

	public void setSub_category_id(String sub_category_id) {
		this.sub_category_id = sub_category_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPurchase_price() {
		return purchase_price;
	}

	public void setPurchase_price(String purchase_price) {
		this.purchase_price = purchase_price;
	}

	public String getSelling_price() {
		return selling_price;
	}

	public void setSelling_price(String selling_price) {
		this.selling_price = selling_price;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getStock_quantity() {
		return stock_quantity;
	}

	public void setStock_quantity(String stock_quantity) {
		this.stock_quantity = stock_quantity;
	}

	@Override
	public String toString() {
		return "Product [product_id=" + product_id + ", serial_number=" + serial_number + ", category_id=" + category_id
				+ ", sub_category_id=" + sub_category_id + ", name=" + name + ", purchase_price=" + purchase_price
				+ ", selling_price=" + selling_price + ", note=" + note + ", stock_quantity=" + stock_quantity + "]";
	}

}
