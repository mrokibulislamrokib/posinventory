package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EmployeerRowMapper implements RowMapper<Employeer> {

	public Employeer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Employeer employeer = new Employeer();
		employeer.setAddress(rs.getString(""));
		employeer.setEmail(rs.getString(""));
		employeer.setName(rs.getString(""));
		employeer.setPassword(rs.getString(""));
		employeer.setPhone(rs.getString(""));
		employeer.setType(rs.getString(""));
		return employeer;
	}

}
