package com.inventory.spring.web.dao;

public class SubCategory {

	private int sub_category_id;
	private String name;
	private String description;
	private String category_id;

	public SubCategory() {
		super();
	}

	public SubCategory(String name, String description, String category_id) {
		super();
		this.name = name;
		this.description = description;
		this.category_id = category_id;
	}

	public SubCategory(int sub_category_id, String name, String description, String category_id) {
		super();
		this.sub_category_id = sub_category_id;
		this.name = name;
		this.description = description;
		this.category_id = category_id;
	}

	public int getSub_category_id() {
		return sub_category_id;
	}

	public void setSub_category_id(int sub_category_id) {
		this.sub_category_id = sub_category_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	@Override
	public String toString() {
		return "SubCategory [sub_category_id=" + sub_category_id + ", name=" + name + ", description=" + description
				+ ", category_id=" + category_id + "]";
	}

}
