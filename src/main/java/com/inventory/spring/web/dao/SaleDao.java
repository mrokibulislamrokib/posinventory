package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class SaleDao {


	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Sale> getSales() {
		return jdbc.query("", new SaleRowMapper());
	}

	public Sale getSale(int id) {
		return (Sale) jdbc.query("", new SaleRowMapper());
	}

	public int[] create(List<Sale> sale) {
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(sale.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(Sale sale) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(sale);
		return jdbc.update("", params) == 1;
	}

	public boolean update(Sale sale) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(sale);
		return jdbc.update("", params) == 1;
	}
	
	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params) == 1;
	}
}
