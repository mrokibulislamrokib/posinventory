package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class UserRowMapper implements RowMapper<User> {

	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user=new User();
		user.setAbout(rs.getString(""));
		user.setEmail(rs.getString(""));
		user.setName(rs.getString(""));
		user.setPassword(rs.getString(""));
		return null;
	}

}
