package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class DamagedProductDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<DamagedProduct> getDamagedProducts() {
		return jdbc.query("", new DamagedProductRowMapper());
	}

	public DamagedProduct getDamagedProduct(int id) {
		return (DamagedProduct) jdbc.query("", new DamagedProductRowMapper());
	}

	public int[] create(List<DamagedProduct> damagedProduct) {
		SqlParameterSource[] params=SqlParameterSourceUtils.createBatch(damagedProduct.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(DamagedProduct damagedProduct) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(damagedProduct);
		return jdbc.update("", params)==1;
	}


	public boolean update(DamagedProduct damagedProduct) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(damagedProduct);
		return jdbc.update("", params)==1;
	}


	public boolean delete(int id) {
		MapSqlParameterSource params=new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params)==1 ;
	}


}
