package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class OrderRowMapper implements RowMapper<Order> {

	public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
		Order order=new Order();
		order.setCreating_timestamp(rs.getString(""));
		order.setCustomer_id(rs.getString(""));
		order.setDiscount_percentage(rs.getString(""));
		order.setDue(rs.getString(""));
		order.setGrand_total(rs.getString(""));
		order.setModifying_timestamp(rs.getString(""));
		order.setNote(rs.getString(""));
		order.setOrder_entries(rs.getString(""));
		order.setOrder_number(rs.getString(""));
		return null;
	}

}
