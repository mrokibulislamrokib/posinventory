package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class PurchaseRowMapper implements RowMapper<Purchase> {

	public Purchase mapRow(ResultSet rs, int rowNum) throws SQLException {
		Purchase purchase=new Purchase();
		purchase.setPurchase_cod(rs.getString(""));
		purchase.setPurchase_entries(rs.getString(""));
		purchase.setSupplier_id(rs.getString(""));
		purchase.setTimestamp(rs.getString(""));
		return purchase;
	}

}
