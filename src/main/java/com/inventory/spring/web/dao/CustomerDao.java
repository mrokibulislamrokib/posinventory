package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class CustomerDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Customer> getCustomers() {
		return jdbc.query("", new CustomerRowMapper());
	}

	public Customer getCustomer(int id) {
		return (Customer) jdbc.query("", new CustomerRowMapper());
	}

	public int[] create(List<Customer> customer) {
		SqlParameterSource[] params=SqlParameterSourceUtils.createBatch(customer.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(Customer customer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(customer);
		return jdbc.update("", params)==1;
	}

	public boolean delete(int id) {
		MapSqlParameterSource params=new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params)==1 ;
	}

	public boolean update(Customer customer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(customer);
		return jdbc.update("", params)==1;
	}

}
