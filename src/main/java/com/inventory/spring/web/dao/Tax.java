package com.inventory.spring.web.dao;

public class Tax {

	private String tax_id;
	private String tax_code;
	private String name;
	private String percentage;
	private String notes;

	public Tax() {
		super();
	}

	public Tax(String tax_code, String name, String percentage, String notes) {
		super();
		this.tax_code = tax_code;
		this.name = name;
		this.percentage = percentage;
		this.notes = notes;
	}

	public Tax(String tax_id, String tax_code, String name, String percentage, String notes) {
		super();
		this.tax_id = tax_id;
		this.tax_code = tax_code;
		this.name = name;
		this.percentage = percentage;
		this.notes = notes;
	}

	public String getTax_id() {
		return tax_id;
	}

	public void setTax_id(String tax_id) {
		this.tax_id = tax_id;
	}

	public String getTax_code() {
		return tax_code;
	}

	public void setTax_code(String tax_code) {
		this.tax_code = tax_code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public String toString() {
		return "Tax [tax_id=" + tax_id + ", tax_code=" + tax_code + ", name=" + name + ", percentage=" + percentage
				+ ", notes=" + notes + "]";
	}

}
