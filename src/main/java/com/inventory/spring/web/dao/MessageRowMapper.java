package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MessageRowMapper implements RowMapper<Message> {

	public Message mapRow(ResultSet rs, int rowNum) throws SQLException {
		Message message=new Message();
		message.setMessage_thread_code(rs.getString(""));
		message.setRead_status(rs.getString(""));
		message.setSender(rs.getString(""));
		message.setTimestamp(rs.getString(""));
		message.setMessage_body(rs.getString(""));
		return message;
	}

}
