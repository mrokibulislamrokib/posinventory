package com.inventory.spring.web.dao;

public class Customer {

	private int customer_id;
	private String customer_code;
	private String name;
	private String email;
	private String password;
	private String phone;
	private String address;
	private String gender;
	private String discount_percentage;

	public Customer() {
		super();
	}

	public Customer(String customer_code, String name, String email, String password, String phone, String address,
			String gender, String discount_percentage) {
		super();
		this.customer_code = customer_code;
		this.name = name;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.address = address;
		this.gender = gender;
		this.discount_percentage = discount_percentage;
	}

	public Customer(int customer_id, String customer_code, String name, String email, String password, String phone,
			String address, String gender, String discount_percentage) {
		super();
		this.customer_id = customer_id;
		this.customer_code = customer_code;
		this.name = name;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.address = address;
		this.gender = gender;
		this.discount_percentage = discount_percentage;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_code() {
		return customer_code;
	}

	public void setCustomer_code(String customer_code) {
		this.customer_code = customer_code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDiscount_percentage() {
		return discount_percentage;
	}

	public void setDiscount_percentage(String discount_percentage) {
		this.discount_percentage = discount_percentage;
	}

	@Override
	public String toString() {
		return "Customer [customer_id=" + customer_id + ", customer_code=" + customer_code + ", name=" + name
				+ ", email=" + email + ", password=" + password + ", phone=" + phone + ", address=" + address
				+ ", gender=" + gender + ", discount_percentage=" + discount_percentage + "]";
	}

}
