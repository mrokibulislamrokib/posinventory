package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SupplierRowMapper implements RowMapper<Supplier> {

	public Supplier mapRow(ResultSet rs, int rowNum) throws SQLException {
		Supplier supplier=new Supplier();
		supplier.setCompany(rs.getString(""));
		supplier.setEmail(rs.getString(""));
		supplier.setName(rs.getString(""));
		supplier.setPhone(rs.getString(""));
		return supplier;
	}

}
