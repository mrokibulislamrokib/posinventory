package com.inventory.spring.web.dao;

public class Supplier {

	private int supplier_id;
	private String company;
	private String name;
	private String email;
	private String phone;

	public Supplier() {
		super();
	}

	public Supplier(String company, String name, String email, String phone) {
		super();
		this.company = company;
		this.name = name;
		this.email = email;
		this.phone = phone;
	}

	public Supplier(int supplier_id, String company, String name, String email, String phone) {
		super();
		this.supplier_id = supplier_id;
		this.company = company;
		this.name = name;
		this.email = email;
		this.phone = phone;
	}

	public int getSupplier_id() {
		return supplier_id;
	}

	public void setSupplier_id(int supplier_id) {
		this.supplier_id = supplier_id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Supplier [supplier_id=" + supplier_id + ", company=" + company + ", name=" + name + ", email=" + email
				+ ", phone=" + phone + "]";
	}

}
