package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ProductRowMapper implements RowMapper<Product> {

	public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
		Product product=new Product();
		product.setName(rs.getString(""));
		product.setNote(rs.getString(""));
		product.setCategory_id(rs.getString(""));
		product.setPurchase_price(rs.getString(""));
		product.setSelling_price(rs.getString(""));
		product.setSerial_number(rs.getString(""));
		product.setStock_quantity(rs.getString(""));
		product.setSub_category_id(rs.getString(""));
		return product;
	}

}
