package com.inventory.spring.web.dao;

public class Employeer {

	private int employee_id;
	private String name;
	private String email;
	private String password;
	private String phone;
	private String address;
	private String type;

	public Employeer() {
		super();
	}

	public Employeer(String name, String email, String password, String phone, String address, String type) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.address = address;
		this.type = type;
	}

	public Employeer(int employee_id, String name, String email, String password, String phone, String address,
			String type) {
		super();
		this.employee_id = employee_id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.address = address;
		this.type = type;
	}

	public int getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Employeer [employee_id=" + employee_id + ", name=" + name + ", email=" + email + ", password="
				+ password + ", phone=" + phone + ", address=" + address + ", type=" + type + "]";
	}

}
