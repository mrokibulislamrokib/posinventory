package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class DamagedProductRowMapper implements RowMapper<DamagedProduct> {

	public DamagedProduct mapRow(ResultSet rs, int rowNum) throws SQLException {
		DamagedProduct damagedproduct=new DamagedProduct();
		damagedproduct.setProduct_id(rs.getString(""));
		damagedproduct.setQuantity(rs.getString(""));
		damagedproduct.setTimestamp(rs.getString(""));
		damagedproduct.setNote(rs.getString(""));
		return damagedproduct;
	}

}
