package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class PurchaseDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Purchase> getPurchases() {
		return jdbc.query("", new PurchaseRowMapper());
	}

	public Purchase getPurchase(int id) {
		return (Purchase) jdbc.query("", new PurchaseRowMapper());
	}

	public int[] create(List<Purchase> purchase) {
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(purchase.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(Purchase purchase) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(purchase);
		return jdbc.update("", params) == 1;
	}

	public boolean update(Purchase purchase) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(purchase);
		return jdbc.update("", params) == 1;
	}

	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params) == 1;
	}
}
