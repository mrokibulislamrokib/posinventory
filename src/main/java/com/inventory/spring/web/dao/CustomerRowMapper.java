package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CustomerRowMapper implements RowMapper<Customer> {

	public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Customer customer=new Customer();
		customer.setCustomer_code(rs.getString(""));
		customer.setEmail(rs.getString(""));
		customer.setGender(rs.getString(""));
		customer.setName(rs.getString(""));
		customer.setPassword(rs.getString(""));
		customer.setPhone(rs.getString(""));
		customer.setAddress(rs.getString(""));
		customer.setDiscount_percentage(rs.getString(""));
		return customer;
	}

}
