package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class SupplierDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Supplier> getSuppliers() {
		return jdbc.query("", new SupplierRowMapper());
	}

	public Supplier getSupplier(int id) {
		return (Supplier) jdbc.query("", new SupplierRowMapper());
	}

	public int[] create(List<Supplier> supplier) {
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(supplier.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(Supplier supplier) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(supplier);
		return jdbc.update("", params) == 1;
	}

	public boolean update(Supplier supplier) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(supplier);
		return jdbc.update("", params) == 1;
	}
	
	
	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params) == 1;
	}
}
