package com.inventory.spring.web.dao;

public class Setting {

	private int settings_id;
	private String type;
	private String description;

	public Setting() {
		super();
	}

	public Setting(String type, String description) {
		super();
		this.type = type;
		this.description = description;
	}

	public Setting(int settings_id, String type, String description) {
		super();
		this.settings_id = settings_id;
		this.type = type;
		this.description = description;
	}

	public int getSettings_id() {
		return settings_id;
	}

	public void setSettings_id(int settings_id) {
		this.settings_id = settings_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Setting [settings_id=" + settings_id + ", type=" + type + ", description=" + description + "]";
	}

}
