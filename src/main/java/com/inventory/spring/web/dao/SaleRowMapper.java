package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SaleRowMapper implements RowMapper<Sale> {

	public Sale mapRow(ResultSet rs, int rowNum) throws SQLException {
		Sale sale=new Sale();
		sale.setCustomer_id(rs.getString(""));
		sale.setDiscount_percentage(rs.getString(""));
		sale.setDue(rs.getString(""));
		sale.setGrand_total(rs.getString(""));
		sale.setInvoice_code(rs.getString(""));
		sale.setInvoice_entries(rs.getString(""));
		sale.setSub_total(rs.getString(""));
		sale.setTimestamp(rs.getString(""));
		sale.setVat_percentage(rs.getString(""));
		return sale;
	}

}
