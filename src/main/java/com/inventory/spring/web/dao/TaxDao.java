package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class TaxDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Tax> getTaxs() {
		return jdbc.query("", new TaxRowMapper());
	}

	public Tax getTax(int id) {
		return (Tax) jdbc.query("", new TaxRowMapper());
	}

	public int[] create(List<Tax> tax) {
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(tax.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(Tax tax) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(tax);
		return jdbc.update("", params) == 1;
	}

	public boolean update(Tax tax) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(tax);
		return jdbc.update("", params) == 1;
	}
	
	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params) == 1;
	}
}
