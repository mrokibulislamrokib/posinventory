package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SubCategoryRowMapper implements RowMapper<SubCategory> {

	public SubCategory mapRow(ResultSet rs, int rowNum) throws SQLException {
		SubCategory subCategory = new SubCategory();
		subCategory.setCategory_id(rs.getString(""));
		subCategory.setDescription(rs.getString(""));
		subCategory.setName(rs.getString(""));
		return subCategory;
	}

}
