package com.inventory.spring.web.dao;

public class Message {

	private int message_id;
	private String message_thread_code;
	private String sender;
	private String message_body;
	private String timestamp;
	private String read_status;

	public Message() {
		super();
	}

	public Message(String message_thread_code, String sender, String message_body, String timestamp,
			String read_status) {
		super();
		this.message_thread_code = message_thread_code;
		this.sender = sender;
		this.message_body = message_body;
		this.timestamp = timestamp;
		this.read_status = read_status;
	}

	public Message(int message_id, String message_thread_code, String sender, String message_body, String timestamp,
			String read_status) {
		super();
		this.message_id = message_id;
		this.message_thread_code = message_thread_code;
		this.sender = sender;
		this.message_body = message_body;
		this.timestamp = timestamp;
		this.read_status = read_status;
	}

	public int getMessage_id() {
		return message_id;
	}

	public void setMessage_id(int message_id) {
		this.message_id = message_id;
	}

	public String getMessage_thread_code() {
		return message_thread_code;
	}

	public void setMessage_thread_code(String message_thread_code) {
		this.message_thread_code = message_thread_code;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getMessage_body() {
		return message_body;
	}

	public void setMessage_body(String message_body) {
		this.message_body = message_body;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getRead_status() {
		return read_status;
	}

	public void setRead_status(String read_status) {
		this.read_status = read_status;
	}

	@Override
	public String toString() {
		return "Message [message_id=" + message_id + ", message_thread_code=" + message_thread_code + ", sender="
				+ sender + ", message_body=" + message_body + ", timestamp=" + timestamp + ", read_status="
				+ read_status + "]";
	}

}
