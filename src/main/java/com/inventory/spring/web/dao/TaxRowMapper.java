package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class TaxRowMapper implements RowMapper<Tax> {

	public Tax mapRow(ResultSet rs, int rowNum) throws SQLException {
		Tax tax=new Tax();
		tax.setName(rs.getString(""));
		tax.setNotes(rs.getString(""));
		tax.setPercentage(rs.getString(""));
		tax.setTax_code(rs.getString(""));
		return tax;
	}

}
