package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class OrderDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Order> getOrders() {
		return jdbc.query("", new OrderRowMapper());
	}

	public Order getOrder(int id) {
		return (Order) jdbc.query("", new OrderRowMapper());
	}

	public int[] create(List<Order> order) {
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(order.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(Order order) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(order);
		return jdbc.update("", params) == 1;
	}

	public boolean update(Order order) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(order);
		return jdbc.update("", params) == 1;
	}
	

	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params) == 1;
	}
}
