package com.inventory.spring.web.dao;

public class Order {

	private int order_id;
	private String order_number;
	private String customer_id;
	private String order_entries;
	private String vat_percentage;
	private String discount_percentage;
	private String sub_total;
	private String grand_total;
	private String due;
	private String shipping_address;
	private String order_status;
	private String payment_status;
	private String note;
	private String creating_timestamp;
	private String modifying_timestamp;

	public Order() {
		super();
	}

	public Order(String order_number, String customer_id, String order_entries, String vat_percentage,
			String discount_percentage, String sub_total, String grand_total, String due, String shipping_address,
			String order_status, String payment_status, String note, String creating_timestamp,
			String modifying_timestamp) {
		super();
		this.order_number = order_number;
		this.customer_id = customer_id;
		this.order_entries = order_entries;
		this.vat_percentage = vat_percentage;
		this.discount_percentage = discount_percentage;
		this.sub_total = sub_total;
		this.grand_total = grand_total;
		this.due = due;
		this.shipping_address = shipping_address;
		this.order_status = order_status;
		this.payment_status = payment_status;
		this.note = note;
		this.creating_timestamp = creating_timestamp;
		this.modifying_timestamp = modifying_timestamp;
	}

	public Order(int order_id, String order_number, String customer_id, String order_entries, String vat_percentage,
			String discount_percentage, String sub_total, String grand_total, String due, String shipping_address,
			String order_status, String payment_status, String note, String creating_timestamp,
			String modifying_timestamp) {
		super();
		this.order_id = order_id;
		this.order_number = order_number;
		this.customer_id = customer_id;
		this.order_entries = order_entries;
		this.vat_percentage = vat_percentage;
		this.discount_percentage = discount_percentage;
		this.sub_total = sub_total;
		this.grand_total = grand_total;
		this.due = due;
		this.shipping_address = shipping_address;
		this.order_status = order_status;
		this.payment_status = payment_status;
		this.note = note;
		this.creating_timestamp = creating_timestamp;
		this.modifying_timestamp = modifying_timestamp;
	}

	public int getOrder_id() {
		return order_id;
	}

	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}

	public String getOrder_number() {
		return order_number;
	}

	public void setOrder_number(String order_number) {
		this.order_number = order_number;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getOrder_entries() {
		return order_entries;
	}

	public void setOrder_entries(String order_entries) {
		this.order_entries = order_entries;
	}

	public String getVat_percentage() {
		return vat_percentage;
	}

	public void setVat_percentage(String vat_percentage) {
		this.vat_percentage = vat_percentage;
	}

	public String getDiscount_percentage() {
		return discount_percentage;
	}

	public void setDiscount_percentage(String discount_percentage) {
		this.discount_percentage = discount_percentage;
	}

	public String getSub_total() {
		return sub_total;
	}

	public void setSub_total(String sub_total) {
		this.sub_total = sub_total;
	}

	public String getGrand_total() {
		return grand_total;
	}

	public void setGrand_total(String grand_total) {
		this.grand_total = grand_total;
	}

	public String getDue() {
		return due;
	}

	public void setDue(String due) {
		this.due = due;
	}

	public String getShipping_address() {
		return shipping_address;
	}

	public void setShipping_address(String shipping_address) {
		this.shipping_address = shipping_address;
	}

	public String getOrder_status() {
		return order_status;
	}

	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}

	public String getPayment_status() {
		return payment_status;
	}

	public void setPayment_status(String payment_status) {
		this.payment_status = payment_status;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCreating_timestamp() {
		return creating_timestamp;
	}

	public void setCreating_timestamp(String creating_timestamp) {
		this.creating_timestamp = creating_timestamp;
	}

	public String getModifying_timestamp() {
		return modifying_timestamp;
	}

	public void setModifying_timestamp(String modifying_timestamp) {
		this.modifying_timestamp = modifying_timestamp;
	}

	@Override
	public String toString() {
		return "Order [order_id=" + order_id + ", order_number=" + order_number + ", customer_id=" + customer_id
				+ ", order_entries=" + order_entries + ", vat_percentage=" + vat_percentage + ", discount_percentage="
				+ discount_percentage + ", sub_total=" + sub_total + ", grand_total=" + grand_total + ", due=" + due
				+ ", shipping_address=" + shipping_address + ", order_status=" + order_status + ", payment_status="
				+ payment_status + ", note=" + note + ", creating_timestamp=" + creating_timestamp
				+ ", modifying_timestamp=" + modifying_timestamp + "]";
	}

}
