package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SettingRowMapper implements RowMapper<Setting> {

	public Setting mapRow(ResultSet rs, int rowNum) throws SQLException {
		Setting setting = new Setting();
		setting.setDescription(rs.getString(""));
		setting.setType(rs.getString(""));
		return setting;
	}

}
