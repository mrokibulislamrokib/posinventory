package com.inventory.spring.web.dao;

public class DamagedProduct {

	private int damaged_product_id;
	private String product_id;
	private String quantity;
	private String note;
	private String timestamp;

	public DamagedProduct() {
		super();
	}

	public DamagedProduct(String product_id, String quantity, String note, String timestamp) {
		super();
		this.product_id = product_id;
		this.quantity = quantity;
		this.note = note;
		this.timestamp = timestamp;
	}

	public DamagedProduct(int damaged_product_id, String product_id, String quantity, String note, String timestamp) {
		super();
		this.damaged_product_id = damaged_product_id;
		this.product_id = product_id;
		this.quantity = quantity;
		this.note = note;
		this.timestamp = timestamp;
	}

	public int getDamaged_product_id() {
		return damaged_product_id;
	}

	public void setDamaged_product_id(int damaged_product_id) {
		this.damaged_product_id = damaged_product_id;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "DamagedProduct [damaged_product_id=" + damaged_product_id + ", product_id=" + product_id + ", quantity="
				+ quantity + ", note=" + note + ", timestamp=" + timestamp + "]";
	}

}
