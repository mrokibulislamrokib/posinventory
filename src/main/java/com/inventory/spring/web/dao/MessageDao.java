package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class MessageDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Message> getMessages() {
		return jdbc.query("", new MessageRowMapper());
	}

	public Message getMessage(int id) {
		return (Message) jdbc.query("", new MessageRowMapper());
	}

	public int[] create(List<Message> message) {
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(message.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(Message message) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(message);
		return jdbc.update("", params) == 1;
	}

	public boolean update(Message message) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(message);
		return jdbc.update("", params) == 1;
	}

	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params) == 1;
	}
}
