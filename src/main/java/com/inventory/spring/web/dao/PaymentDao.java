package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class PaymentDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Payment> getPayments() {
		return jdbc.query("", new PaymentRowMapper());
	}

	public Payment getPayment(int id) {
		return (Payment) jdbc.query("", new PaymentRowMapper());
	}

	public int[] create(List<Payment> payment) {
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(payment.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(Payment payment) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(payment);
		return jdbc.update("", params) == 1;
	}


	public boolean update(Payment payment) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(payment);
		return jdbc.update("", params) == 1;
	}
	

	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params) == 1;
	}
}
