package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class ProductDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Product> getProducts() {
		return jdbc.query("", new ProductRowMapper());
	}

	public Product getProduct(int id) {
		return (Product) jdbc.query("", new ProductRowMapper());
	}

	public int[] create(List<Product> product) {
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(product.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(Product product) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(product);
		return jdbc.update("", params) == 1;
	}

	public boolean update(Product product) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(product);
		return jdbc.update("", params) == 1;
	}
	
	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params) == 1;
	}
}
