package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class EmployeerDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Employeer> getEmployeers() {
		return jdbc.query("", new EmployeerRowMapper());
	}

	public Employeer getEmployeer(int id) {
		return (Employeer) jdbc.query("", new EmployeerRowMapper());
	}

	public int[] create(List<Employeer> employeer) {
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(employeer.toArray());
		return jdbc.batchUpdate("", params);
	}

	public boolean create(Employeer employeer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(employeer);
		return jdbc.update("", params)==1;
	}

	public boolean update(Employeer employeer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(employeer);
		return jdbc.update("", params) == 1;
	}

	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params) == 1;
	}

}
