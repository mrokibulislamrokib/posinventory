package com.inventory.spring.web.dao;

public class User {

	private int admin_id;
	private String name;
	private String email;
	private String password;
	private String about;
	
	public User() {
		super();
	}

	public User(String name, String email, String password, String about) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.about = about;
	}
	
	public User(int admin_id, String name, String email, String password, String about) {
		super();
		this.admin_id = admin_id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.about = about;
	}
	
	public int getAdmin_id() {
		return admin_id;
	}
	
	public void setAdmin_id(int admin_id) {
		this.admin_id = admin_id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getAbout() {
		return about;
	}
	
	public void setAbout(String about) {
		this.about = about;
	}
	
	@Override
	public String toString() {
		return "User [admin_id=" + admin_id + ", name=" + name + ", email=" + email + ", password=" + password
				+ ", about=" + about + "]";
	}
	
}
