package com.inventory.spring.web.dao;

public class Purchase {

	private int purchase_id;
	private String purchase_cod;
	private String supplier_id;
	private String purchase_entries;
	private String timestamp;

	public Purchase() {
		super();
	}

	public Purchase(String purchase_cod, String supplier_id, String purchase_entries, String timestamp) {
		super();
		this.purchase_cod = purchase_cod;
		this.supplier_id = supplier_id;
		this.purchase_entries = purchase_entries;
		this.timestamp = timestamp;
	}

	public Purchase(int purchase_id, String purchase_cod, String supplier_id, String purchase_entries,
			String timestamp) {
		super();
		this.purchase_id = purchase_id;
		this.purchase_cod = purchase_cod;
		this.supplier_id = supplier_id;
		this.purchase_entries = purchase_entries;
		this.timestamp = timestamp;
	}

	public int getPurchase_id() {
		return purchase_id;
	}

	public void setPurchase_id(int purchase_id) {
		this.purchase_id = purchase_id;
	}

	public String getPurchase_cod() {
		return purchase_cod;
	}

	public void setPurchase_cod(String purchase_cod) {
		this.purchase_cod = purchase_cod;
	}

	public String getSupplier_id() {
		return supplier_id;
	}

	public void setSupplier_id(String supplier_id) {
		this.supplier_id = supplier_id;
	}

	public String getPurchase_entries() {
		return purchase_entries;
	}

	public void setPurchase_entries(String purchase_entries) {
		this.purchase_entries = purchase_entries;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "Purchase [purchase_id=" + purchase_id + ", purchase_cod=" + purchase_cod + ", supplier_id="
				+ supplier_id + ", purchase_entries=" + purchase_entries + ", timestamp=" + timestamp + "]";
	}

}
