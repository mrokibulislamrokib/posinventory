package com.inventory.spring.web.dao;

public class Payment {

	private int payment_id;
	private String type;
	private String method;
	private String amount;
	private String timestamp;
	private String supplier_id;
	private String customer_id;
	private String invoice_id;
	private String purchase_id;
	private String order_id;

	public Payment() {
		super();
	}

	public Payment(String type, String method, String amount, String timestamp, String supplier_id, String customer_id,
			String invoice_id, String purchase_id, String order_id) {
		super();
		this.type = type;
		this.method = method;
		this.amount = amount;
		this.timestamp = timestamp;
		this.supplier_id = supplier_id;
		this.customer_id = customer_id;
		this.invoice_id = invoice_id;
		this.purchase_id = purchase_id;
		this.order_id = order_id;
	}

	public Payment(int payment_id, String type, String method, String amount, String timestamp, String supplier_id,
			String customer_id, String invoice_id, String purchase_id, String order_id) {
		super();
		this.payment_id = payment_id;
		this.type = type;
		this.method = method;
		this.amount = amount;
		this.timestamp = timestamp;
		this.supplier_id = supplier_id;
		this.customer_id = customer_id;
		this.invoice_id = invoice_id;
		this.purchase_id = purchase_id;
		this.order_id = order_id;
	}

	public int getPayment_id() {
		return payment_id;
	}

	public void setPayment_id(int payment_id) {
		this.payment_id = payment_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getSupplier_id() {
		return supplier_id;
	}

	public void setSupplier_id(String supplier_id) {
		this.supplier_id = supplier_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getInvoice_id() {
		return invoice_id;
	}

	public void setInvoice_id(String invoice_id) {
		this.invoice_id = invoice_id;
	}

	public String getPurchase_id() {
		return purchase_id;
	}

	public void setPurchase_id(String purchase_id) {
		this.purchase_id = purchase_id;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	@Override
	public String toString() {
		return "Payment [payment_id=" + payment_id + ", type=" + type + ", method=" + method + ", amount=" + amount
				+ ", timestamp=" + timestamp + ", supplier_id=" + supplier_id + ", customer_id=" + customer_id
				+ ", invoice_id=" + invoice_id + ", purchase_id=" + purchase_id + ", order_id=" + order_id + "]";
	}

}
