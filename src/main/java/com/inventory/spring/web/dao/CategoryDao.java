package com.inventory.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

public class CategoryDao {

	private NamedParameterJdbcTemplate jdbc;
	
	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Category> getCategories() {
		return  jdbc.query("",new CategoryRowMapper());
	}
	
	public Category getCategory(int id) {
		return (Category) jdbc.query("",new CategoryRowMapper());
	}
	
	public int[] create(List<Category> categories) {
		SqlParameterSource[] params=SqlParameterSourceUtils.createBatch(categories.toArray());
		return jdbc.batchUpdate("", params);
	}
	
	public boolean create(Category category) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(category);
		return jdbc.update("", params)==1;
	}

	public boolean delete(int id) {
		MapSqlParameterSource params=new MapSqlParameterSource();
		params.addValue("id", id);
		return jdbc.update("", params)==1 ;
	}

	public boolean update(Category category) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(category);
		return jdbc.update("", params)==1;
	}
	
	
}
