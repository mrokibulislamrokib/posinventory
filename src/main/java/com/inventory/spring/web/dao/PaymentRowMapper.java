package com.inventory.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class PaymentRowMapper implements RowMapper<Payment> {

	public Payment mapRow(ResultSet rs, int rowNum) throws SQLException {
		Payment payment=new Payment();
		payment.setAmount(rs.getString(""));
		payment.setCustomer_id(rs.getString(""));
		payment.setInvoice_id(rs.getString(""));
		payment.setOrder_id(rs.getString(""));
		payment.setPurchase_id(rs.getString(""));
		payment.setSupplier_id(rs.getString(""));
		payment.setTimestamp(rs.getString(""));
		payment.setType(rs.getString("")); 
		return payment;
	}

}
