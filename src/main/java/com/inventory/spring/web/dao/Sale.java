package com.inventory.spring.web.dao;

public class Sale {

	private int invoice_id;
	private String invoice_code;
	private String invoice_entries;
	private String discount_percentage;
	private String vat_percentage;
	private String sub_total;
	private String grand_total;
	private String due;
	private String customer_id;
	private String timestamp;

	public Sale() {
		super();
	}

	public Sale(String invoice_code, String invoice_entries, String discount_percentage, String vat_percentage,
			String sub_total, String grand_total, String due, String customer_id, String timestamp) {
		super();
		this.invoice_code = invoice_code;
		this.invoice_entries = invoice_entries;
		this.discount_percentage = discount_percentage;
		this.vat_percentage = vat_percentage;
		this.sub_total = sub_total;
		this.grand_total = grand_total;
		this.due = due;
		this.customer_id = customer_id;
		this.timestamp = timestamp;
	}

	public Sale(int invoice_id, String invoice_code, String invoice_entries, String discount_percentage,
			String vat_percentage, String sub_total, String grand_total, String due, String customer_id,
			String timestamp) {
		super();
		this.invoice_id = invoice_id;
		this.invoice_code = invoice_code;
		this.invoice_entries = invoice_entries;
		this.discount_percentage = discount_percentage;
		this.vat_percentage = vat_percentage;
		this.sub_total = sub_total;
		this.grand_total = grand_total;
		this.due = due;
		this.customer_id = customer_id;
		this.timestamp = timestamp;
	}

	public int getInvoice_id() {
		return invoice_id;
	}

	public void setInvoice_id(int invoice_id) {
		this.invoice_id = invoice_id;
	}

	public String getInvoice_code() {
		return invoice_code;
	}

	public void setInvoice_code(String invoice_code) {
		this.invoice_code = invoice_code;
	}

	public String getInvoice_entries() {
		return invoice_entries;
	}

	public void setInvoice_entries(String invoice_entries) {
		this.invoice_entries = invoice_entries;
	}

	public String getDiscount_percentage() {
		return discount_percentage;
	}

	public void setDiscount_percentage(String discount_percentage) {
		this.discount_percentage = discount_percentage;
	}

	public String getVat_percentage() {
		return vat_percentage;
	}

	public void setVat_percentage(String vat_percentage) {
		this.vat_percentage = vat_percentage;
	}

	public String getSub_total() {
		return sub_total;
	}

	public void setSub_total(String sub_total) {
		this.sub_total = sub_total;
	}

	public String getGrand_total() {
		return grand_total;
	}

	public void setGrand_total(String grand_total) {
		this.grand_total = grand_total;
	}

	public String getDue() {
		return due;
	}

	public void setDue(String due) {
		this.due = due;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "Sale [invoice_id=" + invoice_id + ", invoice_code=" + invoice_code + ", invoice_entries="
				+ invoice_entries + ", discount_percentage=" + discount_percentage + ", vat_percentage="
				+ vat_percentage + ", sub_total=" + sub_total + ", grand_total=" + grand_total + ", due=" + due
				+ ", customer_id=" + customer_id + ", timestamp=" + timestamp + "]";
	}

}
