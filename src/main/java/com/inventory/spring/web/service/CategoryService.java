package com.inventory.spring.web.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.inventory.spring.web.dao.Category;
import com.inventory.spring.web.dao.CategoryDao;

@Service("categoryService")
public class CategoryService {

	private CategoryDao categoryDAO;

	public void setPrductDAO(CategoryDao categoryDAO) {
		this.categoryDAO = categoryDAO;
	}

	public List<Category> getCategory() {
		return categoryDAO.getCategories();
	}

	public void create(Category category) {
		categoryDAO.create(category);
	}
}
