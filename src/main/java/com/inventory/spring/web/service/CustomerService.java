package com.inventory.spring.web.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.inventory.spring.web.dao.CategoryDao;
import com.inventory.spring.web.dao.Customer;
import com.inventory.spring.web.dao.CustomerDao;

@Service("customerService")
public class CustomerService {
	
	private CustomerDao customerDAO;

	public void setPrductDAO(CustomerDao customerDAO) {
		this.customerDAO = customerDAO;
	}

	public List<Customer> getCustomer() {
		return null;
		// return customerDAO.getCustomers();
	}

	public void create(Customer customer) {
		customerDAO.create(customer);
	}
}
